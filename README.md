# RANDALO - TECHNICAL TEST

## Sub modules

- API: `https://gitlab.com/rendalo/api.git`
- App: `https://gitlab.com/rendalo/app.git`

## Getting All At Once

```bash
git clone --recursive https://gitlab.com/rendalo/tech-test.git
```
